import React from 'react'
import ParentSize from '@vx/responsive/lib/components/ParentSize'
import Vxsvgbar from './Vxsvgbar'

const Vxbar = ({ data }) => {
  return (
    <ParentSize>
      {({ width, height }) => (
        <Vxsvgbar width={width} height={height} data={data} />
      )}
    </ParentSize>
  )
}

export default Vxbar
