import React, { useEffect } from "react";
import { useRandomSongQuery } from "./../graphql/generated/graphql";

const RandomSong = () => {
  const { data: { getRandomSong } = {}, refetch } = useRandomSongQuery();

  useEffect(() => {
    const interval = setInterval(() => {
      refetch();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div>
      {getRandomSong && (
        <div
          style={{
            height: "140px",
            width: "140px",
            overflow: "hidden",
            margin: "0 10px 10px 0",
            display: "inline-block",
          }}
        >
          <p
            style={{
              width: "100px",
              whiteSpace: "nowrap",
              overflow: "hidden",
            }}
          >{`${getRandomSong.name}`}</p>
          <img
            src={getRandomSong.image}
            alt={getRandomSong.name}
            style={{ height: "100px", width: "100px" }}
          />
        </div>
      )}
    </div>
  );
};

export default RandomSong;
