import React from "react";
import { ResponsivePie } from "@nivo/pie";

const Nivo = ({ data }) => {
  return (
    <div style={{ height: "500px" }}>
      <ResponsivePie
        data={[
          {
            id: "python",
            label: "python",
            value: 282,
            color: "hsl(101, 70%, 50%)",
          },
          {
            id: "erlang",
            label: "erlang",
            value: 19,
            color: "hsl(29, 70%, 50%)",
          },
          {
            id: "sass",
            label: "sass",
            value: 82,
            color: "hsl(27, 70%, 50%)",
          },
          {
            id: "haskell",
            label: "haskell",
            value: 11,
            color: "hsl(200, 70%, 50%)",
          },
          {
            id: "rust",
            label: "rust",
            value: 69,
            color: "hsl(357, 70%, 50%)",
          },
        ]}
        margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
        innerRadius={0.5}
        padAngle={2}
        cornerRadius={3}
        colors={{ scheme: "nivo" }}
        borderWidth={2}
        borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
        radialLabelsSkipAngle={10}
        radialLabelsTextXOffset={6}
        radialLabelsTextColor="#333333"
        radialLabelsLinkOffset={0}
        radialLabelsLinkDiagonalLength={20}
        radialLabelsLinkHorizontalLength={19}
        radialLabelsLinkStrokeWidth={3}
        radialLabelsLinkColor="black"
        slicesLabelsSkipAngle={10}
        slicesLabelsTextColor="#333333"
        animate={true}
        motionStiffness={90}
        motionDamping={15}
        defs={[
          {
            id: "dots",
            type: "patternDots",
            background: "inherit",
            color: "rgba(255, 255, 255, 0.3)",
            size: 4,
            padding: 1,
            stagger: true,
          },
          {
            id: "lines",
            type: "patternLines",
            background: "inherit",
            color: "rgba(255, 255, 255, 0.3)",
            rotation: -45,
            lineWidth: 6,
            spacing: 10,
          },
        ]}
        fill={[
          {
            match: {
              id: "ruby",
            },
            id: "dots",
          },
          {
            match: {
              id: "c",
            },
            id: "dots",
          },
          {
            match: {
              id: "go",
            },
            id: "dots",
          },
          {
            match: {
              id: "python",
            },
            id: "dots",
          },
          {
            match: {
              id: "scala",
            },
            id: "lines",
          },
          {
            match: {
              id: "lisp",
            },
            id: "lines",
          },
          {
            match: {
              id: "elixir",
            },
            id: "lines",
          },
          {
            match: {
              id: "javascript",
            },
            id: "lines",
          },
        ]}
        legends={[
          {
            anchor: "bottom",
            direction: "row",
            translateY: 56,
            itemWidth: 100,
            itemHeight: 18,
            itemTextColor: "#999",
            symbolSize: 18,
            symbolShape: "circle",
            effects: [
              {
                on: "hover",
                style: {
                  itemTextColor: "#000",
                },
              },
            ],
          },
        ]}
      />
    </div>
  );
};

export default Nivo;
