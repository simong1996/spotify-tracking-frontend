import React, { useEffect, useState } from "react";
import { useSongsQuery } from "./../graphql/generated/graphql";
import { getSongFilter, getPlayedFilter } from "../utils";
import TopFiveCarousel from "./TopFiveCarousel";

const TopSongs = (props) => {
  const [formatedSongs, setFormatedSongs] = useState([]);
  const [topFiveSongs, setTopFiveSongs] = useState([]);
  const { data: { songs } = {} } = useSongsQuery({
    variables: {
      where: getSongFilter(props),
      played: getPlayedFilter(props),
    },
  });

  useEffect(() => {
    if (songs && songs.length > 0) {
      const _formatedSongs = songs
        .map((song) => ({
          name: song.name,
          image: song.image,
          count: song.playeds.length,
        }))
        .sort((a, b) => b.count - a.count);

      setFormatedSongs(_formatedSongs);
      setTopFiveSongs(_formatedSongs.slice(0, 5));
    }
  }, [songs]);

  return (
    <div>
      <div style={{ maxHeight: "450px", overflow: "scroll" }}>
        {formatedSongs &&
          formatedSongs.map((song, index) => (
            <div
              key={index}
              style={{
                height: "140px",
                width: "140px",
                overflow: "hidden",
                margin: "0 10px 10px 0",
                display: "inline-block",
              }}
            >
              <p
                style={{
                  width: "100px",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                }}
              >{`${song.name} ${song.count}`}</p>
              <img
                src={song.image}
                alt={song.name}
                style={{ height: "100px", width: "100px" }}
              />
            </div>
          ))}
      </div>
      {topFiveSongs && <TopFiveCarousel topFive={topFiveSongs} />}
    </div>
  );
};

export default TopSongs;
