import React, { useState, useEffect } from "react";
import Dashboard from "./Dashboard";
import store from "store";

const App = () => {
  const [id, setId] = useState(null);

  const paramId =
    store.get("userId") || new URL(document.location).searchParams.get("id");
  useEffect(() => {
    if (paramId) {
      setId(paramId);
      store.set("userId", paramId);
      window.history.replaceState({}, document.title, "/");
    }
  }, [paramId]);

  return (
    <div className="App">
      <header className="App-header">
        {id ? (
          <Dashboard id={id} />
        ) : (
          <a href={`${process.env.REACT_APP_API_LOGIN}`}>Log in with Spotify</a>
        )}
      </header>
    </div>
  );
};

export default App;
