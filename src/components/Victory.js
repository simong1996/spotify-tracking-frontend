import React, { useState, useEffect } from "react";
import { VictoryPie } from "victory";

const Victory = ({ data: _data }) => {
  //const [angle, setAngle] = useState(0);
  const [data, setData] = useState([
    { x: 0, y: 100 },
    { x: 1, y: 0 },
    { x: 2, y: 0 },
    { x: 3, y: 0 },
    { x: 4, y: 0 },
  ]);
  const [currentLabel, setCurrentLabel] = useState("");
  const [currentPlayed, setCurrentPlayed] = useState("");

  useEffect(() => {
    const total = _data.reduce((t, item) => (t += item.y), 0);
    const newData = _data.map((item) => ({
      ...item,
      y: Math.round((item.y / total) * 100),
      count: Math.round(item.y),
    }));
    setData(newData);
  }, [_data]);

  //   console.log(data);

  return (
    <div style={{ width: "400px" }}>
      <svg width={300} height={300}>
        <text>
          <tspan
            textLength="100px"
            x={100}
            y={134}
            textAnchor="center"
            style={{
              padding: "20px",
              fontSize: "14px",
              letterSpacing: "normal",
              fill: "rgb(37, 37, 37)",
              stroke: "transparent",
            }}
          >
            {currentLabel}
          </tspan>
          <tspan
            textLength="100px"
            x={100}
            y={166}
            textAnchor="center"
            style={{
              padding: "20px",
              fontSize: "14px",
              letterSpacing: "normal",
              fill: "rgb(37, 37, 37)",
              stroke: "transparent",
            }}
          >
            {currentPlayed}
          </tspan>
        </text>
        {/* <circle cx={150} cy={150} r={50} fill="#c43a31"></circle> */}
        <VictoryPie
          animate={{
            duration: 2000,
          }}
          padAngle={5}
          standalone={false}
          width={300}
          height={300}
          innerRadius={75}
          cornerRadius={5}
          data={data}
          labels={() => null}
          events={[
            {
              target: "data",
              eventHandlers: {
                //   onClick: (props) => ({
                //     target: "data",
                //     mutation: (props) => {
                //       console.log(props);
                //     },
                //   }),
                onMouseOver: () => ({
                  target: "data",
                  mutation: ({ datum, style }) => {
                    setCurrentLabel(datum.name);
                    setCurrentPlayed(
                      `played: ${Math.round(datum.count)} (${Math.round(
                        datum.y
                      )}%)`
                    );
                    return {
                      style: Object.assign({}, style, { fill: "tomato" }),
                    };
                  },
                }),
                onMouseOut: () => ({
                  mutation: () => {
                    return null;
                  },
                }),
              },
            },
          ]}
        />
      </svg>
      <p>{currentLabel}</p>
      <p>{currentPlayed}</p>
    </div>
  );
};

export default Victory;
