import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
};

export type Query = {
  __typename?: 'Query';
  user?: Maybe<User>;
  users: Array<User>;
  playeds: Array<Played>;
  songs: Array<Song>;
  genres: Array<Genre>;
  artists: Array<Artist>;
  getRandomSong: ReducedSong;
};


export type QueryUserArgs = {
  where: UserWhereUniqueInput;
};


export type QueryUsersArgs = {
  where?: Maybe<UserWhereInput>;
  orderBy?: Maybe<UserOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
};


export type QueryPlayedsArgs = {
  where?: Maybe<PlayedWhereInput>;
  orderBy?: Maybe<PlayedOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
};


export type QuerySongsArgs = {
  where?: Maybe<SongWhereInput>;
  orderBy?: Maybe<SongOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
};


export type QueryGenresArgs = {
  where?: Maybe<GenreWhereInput>;
  orderBy?: Maybe<GenreOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
};


export type QueryArtistsArgs = {
  where?: Maybe<ArtistWhereInput>;
  orderBy?: Maybe<ArtistOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
};

export type UserWhereUniqueInput = {
  id?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  id: Scalars['String'];
  createdAt: Scalars['DateTime'];
  email: Scalars['String'];
  refreshToken: Scalars['String'];
  accessToken: Scalars['String'];
  after?: Maybe<Scalars['String']>;
  playeds: Array<Played>;
};


export type Played = {
  __typename?: 'Played';
  id: Scalars['String'];
  date: Scalars['DateTime'];
  song: Song;
  user: User;
};

export type Song = {
  __typename?: 'Song';
  id: Scalars['String'];
  name: Scalars['String'];
  duration: Scalars['Int'];
  artists: Array<Artist>;
  genres: Array<Genre>;
  image?: Maybe<Scalars['String']>;
  playeds: Array<Played>;
};


export type SongPlayedsArgs = {
  where?: Maybe<PlayedWhereInput>;
};

export type Artist = {
  __typename?: 'Artist';
  id: Scalars['String'];
  name: Scalars['String'];
  songs: Array<Song>;
  image?: Maybe<Scalars['String']>;
};


export type ArtistSongsArgs = {
  where?: Maybe<SongWhereInput>;
};

export type SongWhereInput = {
  id?: Maybe<StringFilter>;
  name?: Maybe<StringFilter>;
  duration?: Maybe<IntFilter>;
  artists?: Maybe<ArtistFilter>;
  genres?: Maybe<GenreFilter>;
  image?: Maybe<NullableStringFilter>;
  playeds?: Maybe<PlayedFilter>;
  AND?: Maybe<Array<SongWhereInput>>;
  OR?: Maybe<Array<SongWhereInput>>;
  NOT?: Maybe<Array<SongWhereInput>>;
};

export type StringFilter = {
  equals?: Maybe<Scalars['String']>;
  not?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  notIn?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  contains?: Maybe<Scalars['String']>;
  startsWith?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
};

export type IntFilter = {
  equals?: Maybe<Scalars['Int']>;
  not?: Maybe<Scalars['Int']>;
  in?: Maybe<Array<Scalars['Int']>>;
  notIn?: Maybe<Array<Scalars['Int']>>;
  lt?: Maybe<Scalars['Int']>;
  lte?: Maybe<Scalars['Int']>;
  gt?: Maybe<Scalars['Int']>;
  gte?: Maybe<Scalars['Int']>;
};

export type ArtistFilter = {
  every?: Maybe<ArtistWhereInput>;
  some?: Maybe<ArtistWhereInput>;
  none?: Maybe<ArtistWhereInput>;
};

export type ArtistWhereInput = {
  id?: Maybe<StringFilter>;
  name?: Maybe<StringFilter>;
  songs?: Maybe<SongFilter>;
  image?: Maybe<NullableStringFilter>;
  AND?: Maybe<Array<ArtistWhereInput>>;
  OR?: Maybe<Array<ArtistWhereInput>>;
  NOT?: Maybe<Array<ArtistWhereInput>>;
};

export type SongFilter = {
  every?: Maybe<SongWhereInput>;
  some?: Maybe<SongWhereInput>;
  none?: Maybe<SongWhereInput>;
};

export type NullableStringFilter = {
  equals?: Maybe<Scalars['String']>;
  not?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  notIn?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  contains?: Maybe<Scalars['String']>;
  startsWith?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
};

export type GenreFilter = {
  every?: Maybe<GenreWhereInput>;
  some?: Maybe<GenreWhereInput>;
  none?: Maybe<GenreWhereInput>;
};

export type GenreWhereInput = {
  id?: Maybe<StringFilter>;
  name?: Maybe<StringFilter>;
  songs?: Maybe<SongFilter>;
  AND?: Maybe<Array<GenreWhereInput>>;
  OR?: Maybe<Array<GenreWhereInput>>;
  NOT?: Maybe<Array<GenreWhereInput>>;
};

export type PlayedFilter = {
  every?: Maybe<PlayedWhereInput>;
  some?: Maybe<PlayedWhereInput>;
  none?: Maybe<PlayedWhereInput>;
};

export type PlayedWhereInput = {
  id?: Maybe<StringFilter>;
  date?: Maybe<DateTimeFilter>;
  songId?: Maybe<StringFilter>;
  userId?: Maybe<StringFilter>;
  AND?: Maybe<Array<PlayedWhereInput>>;
  OR?: Maybe<Array<PlayedWhereInput>>;
  NOT?: Maybe<Array<PlayedWhereInput>>;
  song?: Maybe<SongWhereInput>;
  user?: Maybe<UserWhereInput>;
};

export type DateTimeFilter = {
  equals?: Maybe<Scalars['DateTime']>;
  not?: Maybe<Scalars['DateTime']>;
  in?: Maybe<Array<Scalars['DateTime']>>;
  notIn?: Maybe<Array<Scalars['DateTime']>>;
  lt?: Maybe<Scalars['DateTime']>;
  lte?: Maybe<Scalars['DateTime']>;
  gt?: Maybe<Scalars['DateTime']>;
  gte?: Maybe<Scalars['DateTime']>;
};

export type UserWhereInput = {
  id?: Maybe<StringFilter>;
  createdAt?: Maybe<DateTimeFilter>;
  email?: Maybe<StringFilter>;
  refreshToken?: Maybe<StringFilter>;
  accessToken?: Maybe<StringFilter>;
  after?: Maybe<NullableStringFilter>;
  playeds?: Maybe<PlayedFilter>;
  AND?: Maybe<Array<UserWhereInput>>;
  OR?: Maybe<Array<UserWhereInput>>;
  NOT?: Maybe<Array<UserWhereInput>>;
};

export type Genre = {
  __typename?: 'Genre';
  id: Scalars['String'];
  name: Scalars['String'];
  songs: Array<Song>;
};


export type GenreSongsArgs = {
  where?: Maybe<SongWhereInput>;
};

export type UserOrderByInput = {
  id?: Maybe<OrderByArg>;
  createdAt?: Maybe<OrderByArg>;
  email?: Maybe<OrderByArg>;
  refreshToken?: Maybe<OrderByArg>;
  accessToken?: Maybe<OrderByArg>;
  after?: Maybe<OrderByArg>;
};

export enum OrderByArg {
  Asc = 'asc',
  Desc = 'desc'
}

export type PlayedOrderByInput = {
  id?: Maybe<OrderByArg>;
  date?: Maybe<OrderByArg>;
  songId?: Maybe<OrderByArg>;
  userId?: Maybe<OrderByArg>;
};

export type SongOrderByInput = {
  id?: Maybe<OrderByArg>;
  name?: Maybe<OrderByArg>;
  duration?: Maybe<OrderByArg>;
  image?: Maybe<OrderByArg>;
};

export type GenreOrderByInput = {
  id?: Maybe<OrderByArg>;
  name?: Maybe<OrderByArg>;
};

export type ArtistOrderByInput = {
  id?: Maybe<OrderByArg>;
  name?: Maybe<OrderByArg>;
  image?: Maybe<OrderByArg>;
};

export type ReducedSong = {
  __typename?: 'ReducedSong';
  id: Scalars['String'];
  name: Scalars['String'];
  duration: Scalars['Int'];
  image: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createUser: User;
  updateUser: User;
  addPlayed: Played;
};


export type MutationCreateUserArgs = {
  email?: Maybe<Scalars['String']>;
  refreshToken?: Maybe<Scalars['String']>;
  accessToken?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
};


export type MutationUpdateUserArgs = {
  userid?: Maybe<Scalars['String']>;
  accessToken?: Maybe<Scalars['String']>;
  refreshToken?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
};


export type MutationAddPlayedArgs = {
  userid?: Maybe<Scalars['String']>;
  date?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  duration?: Maybe<Scalars['Int']>;
  image?: Maybe<Scalars['String']>;
  artists?: Maybe<Array<Scalars['String']>>;
  artistimg?: Maybe<Array<Scalars['String']>>;
  genres?: Maybe<Array<Scalars['String']>>;
};

export type ArtistFragmentFragment = (
  { __typename?: 'Artist' }
  & Pick<Artist, 'id' | 'name' | 'image'>
);

export type GenreFragmentFragment = (
  { __typename?: 'Genre' }
  & Pick<Genre, 'id' | 'name'>
);

export type PlayedFragmentFragment = (
  { __typename?: 'Played' }
  & Pick<Played, 'id' | 'date'>
  & { song: (
    { __typename?: 'Song' }
    & SongFragmentFragment
  ) }
);

export type ReducedSongFragmentFragment = (
  { __typename?: 'ReducedSong' }
  & Pick<ReducedSong, 'id' | 'name' | 'duration' | 'image'>
);

export type SongFragmentFragment = (
  { __typename?: 'Song' }
  & Pick<Song, 'id' | 'name' | 'duration' | 'image'>
  & { artists: Array<(
    { __typename?: 'Artist' }
    & ArtistFragmentFragment
  )>, genres: Array<(
    { __typename?: 'Genre' }
    & GenreFragmentFragment
  )> }
);

export type UserFragmentFragment = (
  { __typename?: 'User' }
  & Pick<User, 'id' | 'email'>
);

export type ArtistsQueryVariables = Exact<{
  where?: Maybe<ArtistWhereInput>;
  song?: Maybe<SongWhereInput>;
  played?: Maybe<PlayedWhereInput>;
}>;


export type ArtistsQuery = (
  { __typename?: 'Query' }
  & { artists: Array<(
    { __typename?: 'Artist' }
    & { songs: Array<(
      { __typename?: 'Song' }
      & Pick<Song, 'name'>
      & { playeds: Array<(
        { __typename?: 'Played' }
        & Pick<Played, 'id'>
      )> }
    )> }
    & ArtistFragmentFragment
  )> }
);

export type GenresQueryVariables = Exact<{
  where?: Maybe<GenreWhereInput>;
  song?: Maybe<SongWhereInput>;
  played?: Maybe<PlayedWhereInput>;
}>;


export type GenresQuery = (
  { __typename?: 'Query' }
  & { genres: Array<(
    { __typename?: 'Genre' }
    & { songs: Array<(
      { __typename?: 'Song' }
      & Pick<Song, 'name'>
      & { playeds: Array<(
        { __typename?: 'Played' }
        & Pick<Played, 'id'>
      )> }
    )> }
    & GenreFragmentFragment
  )> }
);

export type PlayedsQueryVariables = Exact<{
  where?: Maybe<PlayedWhereInput>;
}>;


export type PlayedsQuery = (
  { __typename?: 'Query' }
  & { playeds: Array<(
    { __typename?: 'Played' }
    & PlayedFragmentFragment
  )> }
);

export type RandomSongQueryVariables = Exact<{ [key: string]: never; }>;


export type RandomSongQuery = (
  { __typename?: 'Query' }
  & { getRandomSong: (
    { __typename?: 'ReducedSong' }
    & ReducedSongFragmentFragment
  ) }
);

export type SongsQueryVariables = Exact<{
  where?: Maybe<SongWhereInput>;
  played?: Maybe<PlayedWhereInput>;
}>;


export type SongsQuery = (
  { __typename?: 'Query' }
  & { songs: Array<(
    { __typename?: 'Song' }
    & { playeds: Array<(
      { __typename?: 'Played' }
      & Pick<Played, 'id' | 'date'>
    )> }
    & SongFragmentFragment
  )> }
);

export type UserQueryVariables = Exact<{
  where: UserWhereUniqueInput;
}>;


export type UserQuery = (
  { __typename?: 'Query' }
  & { user?: Maybe<(
    { __typename?: 'User' }
    & UserFragmentFragment
  )> }
);

export const ArtistFragmentFragmentDoc = Apollo.gql`
    fragment ArtistFragment on Artist {
  id
  name
  image
}
    `;
export const GenreFragmentFragmentDoc = Apollo.gql`
    fragment GenreFragment on Genre {
  id
  name
}
    `;
export const SongFragmentFragmentDoc = Apollo.gql`
    fragment SongFragment on Song {
  id
  name
  duration
  image
  artists {
    ...ArtistFragment
  }
  genres {
    ...GenreFragment
  }
}
    ${ArtistFragmentFragmentDoc}
${GenreFragmentFragmentDoc}`;
export const PlayedFragmentFragmentDoc = Apollo.gql`
    fragment PlayedFragment on Played {
  id
  date
  song {
    ...SongFragment
  }
}
    ${SongFragmentFragmentDoc}`;
export const ReducedSongFragmentFragmentDoc = Apollo.gql`
    fragment ReducedSongFragment on ReducedSong {
  id
  name
  duration
  image
}
    `;
export const UserFragmentFragmentDoc = Apollo.gql`
    fragment UserFragment on User {
  id
  email
}
    `;
export const ArtistsDocument = Apollo.gql`
    query Artists($where: ArtistWhereInput, $song: SongWhereInput, $played: PlayedWhereInput) {
  artists(where: $where) {
    ...ArtistFragment
    songs(where: $song) {
      name
      playeds(where: $played) {
        id
      }
    }
  }
}
    ${ArtistFragmentFragmentDoc}`;

/**
 * __useArtistsQuery__
 *
 * To run a query within a React component, call `useArtistsQuery` and pass it any options that fit your needs.
 * When your component renders, `useArtistsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useArtistsQuery({
 *   variables: {
 *      where: // value for 'where'
 *      song: // value for 'song'
 *      played: // value for 'played'
 *   },
 * });
 */
export function useArtistsQuery(baseOptions?: Apollo.QueryHookOptions<ArtistsQuery, ArtistsQueryVariables>) {
        return Apollo.useQuery<ArtistsQuery, ArtistsQueryVariables>(ArtistsDocument, baseOptions);
      }
export function useArtistsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ArtistsQuery, ArtistsQueryVariables>) {
          return Apollo.useLazyQuery<ArtistsQuery, ArtistsQueryVariables>(ArtistsDocument, baseOptions);
        }
export type ArtistsQueryHookResult = ReturnType<typeof useArtistsQuery>;
export type ArtistsLazyQueryHookResult = ReturnType<typeof useArtistsLazyQuery>;
export type ArtistsQueryResult = Apollo.QueryResult<ArtistsQuery, ArtistsQueryVariables>;
export const GenresDocument = Apollo.gql`
    query Genres($where: GenreWhereInput, $song: SongWhereInput, $played: PlayedWhereInput) {
  genres(where: $where) {
    ...GenreFragment
    songs(where: $song) {
      name
      playeds(where: $played) {
        id
      }
    }
  }
}
    ${GenreFragmentFragmentDoc}`;

/**
 * __useGenresQuery__
 *
 * To run a query within a React component, call `useGenresQuery` and pass it any options that fit your needs.
 * When your component renders, `useGenresQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGenresQuery({
 *   variables: {
 *      where: // value for 'where'
 *      song: // value for 'song'
 *      played: // value for 'played'
 *   },
 * });
 */
export function useGenresQuery(baseOptions?: Apollo.QueryHookOptions<GenresQuery, GenresQueryVariables>) {
        return Apollo.useQuery<GenresQuery, GenresQueryVariables>(GenresDocument, baseOptions);
      }
export function useGenresLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GenresQuery, GenresQueryVariables>) {
          return Apollo.useLazyQuery<GenresQuery, GenresQueryVariables>(GenresDocument, baseOptions);
        }
export type GenresQueryHookResult = ReturnType<typeof useGenresQuery>;
export type GenresLazyQueryHookResult = ReturnType<typeof useGenresLazyQuery>;
export type GenresQueryResult = Apollo.QueryResult<GenresQuery, GenresQueryVariables>;
export const PlayedsDocument = Apollo.gql`
    query Playeds($where: PlayedWhereInput) {
  playeds(where: $where) {
    ...PlayedFragment
  }
}
    ${PlayedFragmentFragmentDoc}`;

/**
 * __usePlayedsQuery__
 *
 * To run a query within a React component, call `usePlayedsQuery` and pass it any options that fit your needs.
 * When your component renders, `usePlayedsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePlayedsQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function usePlayedsQuery(baseOptions?: Apollo.QueryHookOptions<PlayedsQuery, PlayedsQueryVariables>) {
        return Apollo.useQuery<PlayedsQuery, PlayedsQueryVariables>(PlayedsDocument, baseOptions);
      }
export function usePlayedsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PlayedsQuery, PlayedsQueryVariables>) {
          return Apollo.useLazyQuery<PlayedsQuery, PlayedsQueryVariables>(PlayedsDocument, baseOptions);
        }
export type PlayedsQueryHookResult = ReturnType<typeof usePlayedsQuery>;
export type PlayedsLazyQueryHookResult = ReturnType<typeof usePlayedsLazyQuery>;
export type PlayedsQueryResult = Apollo.QueryResult<PlayedsQuery, PlayedsQueryVariables>;
export const RandomSongDocument = Apollo.gql`
    query RandomSong {
  getRandomSong {
    ...ReducedSongFragment
  }
}
    ${ReducedSongFragmentFragmentDoc}`;

/**
 * __useRandomSongQuery__
 *
 * To run a query within a React component, call `useRandomSongQuery` and pass it any options that fit your needs.
 * When your component renders, `useRandomSongQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRandomSongQuery({
 *   variables: {
 *   },
 * });
 */
export function useRandomSongQuery(baseOptions?: Apollo.QueryHookOptions<RandomSongQuery, RandomSongQueryVariables>) {
        return Apollo.useQuery<RandomSongQuery, RandomSongQueryVariables>(RandomSongDocument, baseOptions);
      }
export function useRandomSongLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RandomSongQuery, RandomSongQueryVariables>) {
          return Apollo.useLazyQuery<RandomSongQuery, RandomSongQueryVariables>(RandomSongDocument, baseOptions);
        }
export type RandomSongQueryHookResult = ReturnType<typeof useRandomSongQuery>;
export type RandomSongLazyQueryHookResult = ReturnType<typeof useRandomSongLazyQuery>;
export type RandomSongQueryResult = Apollo.QueryResult<RandomSongQuery, RandomSongQueryVariables>;
export const SongsDocument = Apollo.gql`
    query Songs($where: SongWhereInput, $played: PlayedWhereInput) {
  songs(where: $where) {
    ...SongFragment
    playeds(where: $played) {
      id
      date
    }
  }
}
    ${SongFragmentFragmentDoc}`;

/**
 * __useSongsQuery__
 *
 * To run a query within a React component, call `useSongsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSongsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSongsQuery({
 *   variables: {
 *      where: // value for 'where'
 *      played: // value for 'played'
 *   },
 * });
 */
export function useSongsQuery(baseOptions?: Apollo.QueryHookOptions<SongsQuery, SongsQueryVariables>) {
        return Apollo.useQuery<SongsQuery, SongsQueryVariables>(SongsDocument, baseOptions);
      }
export function useSongsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SongsQuery, SongsQueryVariables>) {
          return Apollo.useLazyQuery<SongsQuery, SongsQueryVariables>(SongsDocument, baseOptions);
        }
export type SongsQueryHookResult = ReturnType<typeof useSongsQuery>;
export type SongsLazyQueryHookResult = ReturnType<typeof useSongsLazyQuery>;
export type SongsQueryResult = Apollo.QueryResult<SongsQuery, SongsQueryVariables>;
export const UserDocument = Apollo.gql`
    query User($where: UserWhereUniqueInput!) {
  user(where: $where) {
    ...UserFragment
  }
}
    ${UserFragmentFragmentDoc}`;

/**
 * __useUserQuery__
 *
 * To run a query within a React component, call `useUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useUserQuery(baseOptions?: Apollo.QueryHookOptions<UserQuery, UserQueryVariables>) {
        return Apollo.useQuery<UserQuery, UserQueryVariables>(UserDocument, baseOptions);
      }
export function useUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserQuery, UserQueryVariables>) {
          return Apollo.useLazyQuery<UserQuery, UserQueryVariables>(UserDocument, baseOptions);
        }
export type UserQueryHookResult = ReturnType<typeof useUserQuery>;
export type UserLazyQueryHookResult = ReturnType<typeof useUserLazyQuery>;
export type UserQueryResult = Apollo.QueryResult<UserQuery, UserQueryVariables>;