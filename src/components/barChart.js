import React from 'react'
import '../barchart.css'

const BarChart = () => {
  return (
    <div
      style={{
        width: '500px',
        height: '500px',
        border: '1px solid black',
        margin: '10px',
        padding: '45px',
        display: 'flex',
        alignItems: 'flex-end',
      }}>
      {/* <div
        style={{
          width: '100px',
          height: '100%',
          margin: '0 auto',
          display: 'flex',
        }}>
        <div
          style={{
            width: '33px',
            height: '100%',
            border: '3px solid black',
            position: 'relative',
            left: '2px',
            transform: 'perspective(120px) rotateY(-3deg)',
            borderRadius: '5px 0 0 5px',
            backgroundColor: 'purple',
          }}></div>
        <div
          style={{
            width: '50px',
            height: '100%',
            border: '3px solid black',
            position: 'relative',
            transform: 'perspective(120px) rotateY(2deg)',
            borderRadius: '0 5px 5px 0',
            backgroundColor: 'purple',
          }}></div>
      </div>
      <div
        style={{
          width: '100px',
          height: '100%',
          margin: '0 auto',
          display: 'flex',
        }}>
        <div
          style={{
            width: '33px',
            height: '100%',
            border: '3px solid black',
            position: 'relative',
            left: '2px',
            transform: 'perspective(120px) rotateY(-3deg)',
            borderRadius: '5px 0 0 5px',
            backgroundColor: 'green',
          }}></div>
        <div
          style={{
            width: '50px',
            height: '100%',
            border: '3px solid black',
            position: 'relative',
            transform: 'perspective(120px) rotateY(2deg)',
            borderRadius: '0 5px 5px 0',
            backgroundColor: 'green',
          }}></div>
      </div> */}

      <div
        className="barContainer1"
        style={{
          //   width: '100px',
          //   height: '100%',
          //   margin: '0 auto',
          //   display: 'flex',
          //   flexDirection: 'column',
          //   justifyContent: 'flex-end',
          zIndex: 99,
          height: '80%',
        }}>
        <div
          style={{
            height: '16px',
            width: '49px',
            background: 'orange',
            //border: '1px solid red',
            transform: 'translate(23px, -1px) skew(70deg, 0deg)',
          }}></div>
        <div style={{ width: '100%', height: '100%', display: 'flex' }}>
          <div
            className="sidebar1"
            style={
              {
                //   width: '45px',
                //   height: '100%',
                //   background: 'green',
                //   //border: '1px solid red',
                //   transform: 'skew(0deg, 20deg) translate(1px, -9px)',
              }
            }></div>
          <div
            className="mainbar1"
            style={
              {
                //   width: '50px',
                //   height: '100%',
                //   background: 'orange',
                //border: '1px solid red',
              }
            }></div>
        </div>
      </div>
      <div
        className="barContainer"
        style={{
          //   width: '100px',
          //   height: '100%',
          //   margin: '0 auto',
          //   display: 'flex',
          //   flexDirection: 'column',
          //   justifyContent: 'flex-end',
          position: 'relative',
          right: '49px',
        }}>
        <div
          style={{
            height: '16px',
            width: '49px',
            background: 'purple',
            //border: '1px solid red',
            transform: 'translate(23px, -1px) skew(70deg, 0deg)',
          }}></div>
        <div style={{ width: '100%', height: '100%', display: 'flex' }}>
          <div
            className="sidebar"
            style={
              {
                //   width: '45px',
                //   height: '100%',
                //   background: 'green',
                //   //border: '1px solid red',
                //   transform: 'skew(0deg, 20deg) translate(1px, -9px)',
              }
            }></div>
          <div
            className="mainbar"
            style={
              {
                //   width: '50px',
                //   height: '100%',
                //   background: 'orange',
                //border: '1px solid red',
              }
            }></div>
        </div>
      </div>
      {/* <div
        style={{
          width: '50px',
          height: '100%',
          border: '1px solid red',
        }}></div> */}
    </div>
  )
}

export default BarChart
