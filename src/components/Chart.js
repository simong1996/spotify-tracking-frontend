import React from "react";
import { Doughnut } from "react-chartjs-2";

const Chart = () => {
  return (
    <div style={{ backgroundColor: "green" }}>
      <Doughnut
        data={{
          labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
          datasets: [
            {
              label: "# of Votes",
              data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                "rgba(255, 99, 132, 0.8)",
                "rgba(54, 162, 235, 0.8)",
                "rgba(255, 206, 86, 0.8)",
                "rgba(75, 192, 192, 0.8)",
                "rgba(153, 102, 255, 0.8)",
                "rgba(255, 159, 64, 0.8)",
              ],
              // borderColor: [
              //   "rgba(255, 99, 132, 0)",
              //   "rgba(54, 162, 235, 0)",
              //   "rgba(255, 206, 86, 0)",
              //   "rgba(75, 192, 192, 0)",
              //   "rgba(153, 102, 255, 0)",
              //   "rgba(255, 159, 64, 0)",
              // ],
              // borderWidth: 1,
            },
          ],
        }}
        options={{
          responsive: true,
          maintainAspectRatio: true,
          cutoutPercentage: 65,
          //circumference: 1 * Math.PI,
          animation: {
            //animateRotate: false,
            animateScale: true,
          },
          elements: {
            arc: {
              borderWidth: 2,
              borderColor: "green",
              borderAlign: "inner",
            },
          },
          legend: {
            display: false,
          },
          tooltips: {
            enabled: false,
          },
        }}
      ></Doughnut>
    </div>
  );
};

export default Chart;
