import React, { useState, useEffect } from 'react'
import { useGenresQuery } from '@generated/graphql'
import { getGenreArtistFilter, getSongFilter, getPlayedFilter } from '../utils'
import TopFiveCarousel from './TopFiveCarousel'
import Vis from './Vis'
import Nivo from './Nivo'
import NivoResponsive from './NivoResponsive'
import Victory from './Victory'
import Recharts from './Recharts'
import Vx from './Vx'
import Vxbar from './Vxbar'
import Canvas from './Canvas'
import Chart from './Chart'
import BarChart from './barChart'

const TopGenres = (props) => {
  const [formatedGenres, setFormatedGenres] = useState([])
  const [topFiveGenres, setTopFiveGenres] = useState([])
  const [victoryPie, setVictoryPie] = useState([])
  const [visPie, setVisPie] = useState([])
  const [nivoPie, setNivoPie] = useState([])
  const [rechartPie, setRechartPie] = useState([])
  const [vxPie, setVxPie] = useState([])
  const { data: { genres } = {} } = useGenresQuery({
    variables: {
      where: getGenreArtistFilter(props),
      song: getSongFilter(props),
      played: getPlayedFilter(props),
    },
  })

  useEffect(() => {
    if (genres && genres.length > 0) {
      const _formatedGenres = genres
        .map((genre) => {
          const count = genre.songs.reduce(
            (total, song) => total + song.playeds.length,
            0,
          )
          return { name: genre.name, count }
        })
        .sort((a, b) => b.count - a.count)
      setFormatedGenres(_formatedGenres)
      setTopFiveGenres(_formatedGenres.slice(0, 5))
      const test = _formatedGenres
        .slice(0, 5)
        .map((genre, i) => ({ x: i, y: genre.count, name: genre.name }))
      const test2 = _formatedGenres
        .slice(0, 5)
        .map((genre) => ({ theta: genre.count, name: genre.name }))
      const test3 = _formatedGenres
        .slice(0, 5)
        .map((genre) => ({ value: genre.count, id: genre.name }))
      const test4 = _formatedGenres
        .slice(0, 5)
        .map((genre) => ({ value: genre.count, name: genre.name }))
      const test5 = _formatedGenres
        .slice(0, 5)
        .map((genre) => ({ usage: genre.count, label: genre.name }))

      setVictoryPie(test)
      setVisPie(test2)
      setNivoPie(test3)
      setRechartPie(test4)
      setVxPie(test5)
    }
  }, [genres])

  return (
    <div>
      {/* <Chart />
      <Canvas /> */}
      <BarChart />
      {/* <div style={{ width: '500px', height: '500px' }}>
        {vxPie && <Vxbar data={vxPie} />}
      </div>
      <div style={{ width: '500px', height: '500px' }}>
        {vxPie && <Vx data={vxPie} />}
      </div> */}
      {/* {rechartPie && <Recharts data={rechartPie} />}
      {nivoPie && <Nivo data={nivoPie} />}
      {nivoPie && <NivoResponsive data={nivoPie} />}
      {victoryPie && victoryPie.length > 0 && <Victory data={victoryPie} />}
      {visPie && visPie.length > 0 && <Vis data={visPie} />} */}
      {/* <div style={{ maxHeight: "300px", overflow: "scroll" }}>
        {formatedGenres &&
          formatedGenres.map((genre, index) => (
            <p key={index}>{`${genre.name} ${genre.count}`}</p>
          ))}
      </div>*/}
      {topFiveGenres && <TopFiveCarousel topFive={topFiveGenres} />}
    </div>
  )
}

export default TopGenres
