import React, { useState, useEffect } from "react";
import { useUserQuery } from "./../graphql/generated/graphql";
import Graph from "./Graph";
import DatePicker from "react-datepicker";
import TopSongs from "./TopSongs";
import TopGenres from "./TopGenres";
import TopArtists from "./TopArtists";
import RandomSong from "./RandomSong";

const Dashboard = ({ id: userId }) => {
  const [user, setUser] = useState(null);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  var today = new Date();
  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);

  const { data } = useUserQuery({
    variables: {
      where: {
        id: userId,
      },
    },
  });

  useEffect(() => {
    setUser(data);
  }, [data]);

  return (
    <div>
      <div>
        <DatePicker
          dateFormat="dd/MM/yyyy"
          selected={startDate}
          onChange={(date) => setStartDate(date)}
          selectsStart
          isClearable
          startDate={startDate}
          endDate={endDate}
          maxDate={today}
          placeholderText="Date range start"
        />
        <DatePicker
          dateFormat="dd/MM/yyyy"
          selected={endDate}
          onChange={(date) => setEndDate(date)}
          selectsEnd
          isClearable
          startDate={startDate}
          endDate={endDate}
          minDate={startDate}
          maxDate={tomorrow}
          placeholderText="Date range end"
        />
      </div>
      <p>loogged in</p>
      {user?.user?.email && (
        <div>
          {/* <Graph
            email={user.user.email}
            startDate={startDate}
            endDate={endDate}
          />
          <TopSongs
            email={user.user.email}
            startDate={startDate}
            endDate={endDate}
          /> */}
          <TopGenres
            email={user.user.email}
            startDate={startDate}
            endDate={endDate}
          />
          {/* <TopArtists
            email={user.user.email}
            startDate={startDate}
            endDate={endDate}
          />
          <RandomSong /> */}
        </div>
      )}
    </div>
  );
};

export default Dashboard;
