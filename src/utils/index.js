export const getPlayedFilter = ({ email, startDate, endDate }) => ({
  AND: [
    {
      user: {
        email: {
          equals: email,
        },
      },
    },
    ...(startDate && endDate
      ? [
          {
            date: {
              gte: startDate,
            },
          },
          {
            date: {
              lte: endDate,
            },
          },
        ]
      : []),
  ],
});

export const getSongFilter = (props) => ({
  playeds: {
    some: getPlayedFilter(props),
  },
});

export const getGenreArtistFilter = (props) => ({
  songs: {
    some: getSongFilter(props),
  },
});
