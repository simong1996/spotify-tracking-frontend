import React, { useState, useEffect } from 'react'
import '../carousel.css'

const TopFiveCarousel = ({ topFive }) => {
  const [width, setWidth] = useState(20)
  const [totalWidth, setTotalWidth] = useState(20)
  const [margin /*setMargin*/] = useState(20)
  const [currIndex, setCurrIndex] = useState(0)

  useEffect(() => {
    resize()
    move(1)
  }, [topFive])

  useEffect(() => {
    const interval = setInterval(() => {
      move(currIndex + 1)
    }, 50000)
    return () => clearInterval(interval)
  }, [currIndex, topFive])

  const resize = () => {
    setWidth(Math.max(window.innerWidth * 0.25, 275))
    setTotalWidth(Math.max(window.innerWidth * 0.25, 275) * topFive.length)
  }

  const move = (index) => {
    if (index < 1) index = topFive.length
    if (index > topFive.length) index = 1
    setCurrIndex(index)
  }

  const prev = () => {
    move(currIndex - 1)
  }

  const next = () => {
    move(currIndex + 1)
  }

  return (
    <div className="carousel">
      <div className="carousel__body">
        <div className="carousel__prev" onClick={() => prev()}>
          <i className="far fa-angle-left">{'<'}</i>
        </div>
        <div className="carousel__next" onClick={() => next()}>
          <i className="far fa-angle-right">{'>'}</i>
        </div>
        <div
          className="carousel__slider"
          style={{
            width: `${totalWidth}px`,
            transform: `translate3d(${
              currIndex * -width + width / 2 + window.innerWidth / 2
            }px, 0, 0)`,
          }}>
          {topFive &&
            topFive.map((item, index) => (
              <div
                key={index}
                className={`carousel__slider__item ${
                  index === currIndex - 1
                    ? 'carousel__slider__item--active'
                    : ''
                }`}
                style={{
                  width: `${width - margin * 2}px`,
                  height: `${width - margin * 2}px`,
                }}>
                <div
                  className="item__3d-frame"
                  style={{
                    transform:
                      index === currIndex - 1
                        ? 'perspective(1200px)'
                        : `perspective(1200px) rotateY(${
                            index < currIndex - 1 ? 40 : -40
                          }deg`,
                  }}>
                  <div
                    className="item__3d-frame__box item__3d-frame__box--front"
                    style={{
                      backgroundImage: item.image
                        ? `url(${item.image})`
                        : '#1e272e',
                    }}>
                    <h1>
                      {index + 1} {item.name}
                    </h1>
                  </div>
                  <div
                    className="item__3d-frame__box item__3d-frame__box--left"
                    style={{
                      backgroundImage: item.image
                        ? `url(${item.image})`
                        : '#1e272e',
                    }}></div>
                  <div
                    className="item__3d-frame__box item__3d-frame__box--right"
                    style={{
                      backgroundImage: item.image
                        ? `url(${item.image})`
                        : '#1e272e',
                    }}></div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  )
}

export default TopFiveCarousel
