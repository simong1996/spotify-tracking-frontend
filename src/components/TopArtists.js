import React, { useState, useEffect } from "react";
import { useArtistsQuery } from "./../graphql/generated/graphql";
import { getGenreArtistFilter, getSongFilter, getPlayedFilter } from "../utils";
import TopFiveCarousel from "./TopFiveCarousel";

const TopArtists = (props) => {
  const [formatedArtists, setFormatedArtists] = useState([]);
  const [topFiveArtists, setTopFiveArtists] = useState([]);
  const { data: { artists } = {} } = useArtistsQuery({
    variables: {
      where: getGenreArtistFilter(props),
      song: getSongFilter(props),
      played: getPlayedFilter(props),
    },
  });

  useEffect(() => {
    if (artists && artists.length > 0) {
      const _formatedArtists = artists
        .map((artist) => {
          const count = artist.songs.reduce(
            (total, song) => total + song.playeds.length,
            0
          );
          return { name: artist.name, image: artist.image, count };
        })
        .sort((a, b) => b.count - a.count);
      setFormatedArtists(_formatedArtists);
      setTopFiveArtists(_formatedArtists.slice(0, 5));
    }
  }, [artists]);

  return (
    <div>
      <div style={{ maxHeight: "450px", overflow: "scroll" }}>
        {formatedArtists &&
          formatedArtists.map((artist, index) => (
            <div
              key={index}
              style={{
                height: "140px",
                width: "140px",
                overflow: "hidden",
                margin: "0 10px 10px 0",
                display: "inline-block",
              }}
            >
              <p
                style={{
                  width: "100px",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                }}
              >{`${artist.name} ${artist.count}`}</p>
              <img
                src={artist.image === "Default Image" ? "" : artist.image}
                alt={artist.name}
                style={{ height: "100px", width: "100px" }}
              />
            </div>
          ))}
      </div>
      {topFiveArtists && <TopFiveCarousel topFive={topFiveArtists} />}
    </div>
  );
};

export default TopArtists;
