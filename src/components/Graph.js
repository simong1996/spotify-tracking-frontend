import React, { useState, useEffect } from "react";
import { usePlayedsQuery } from "./../graphql/generated/graphql";
import Stopwatch from "./Stopwatch";
import { getPlayedFilter } from "../utils";

const Graph = (props) => {
  const { startDate, endDate } = props;

  const [minDay, setMinDay] = useState(null);
  const [avgContext, setAvgContext] = useState({
    week: "",
    month: "",
    year: "",
  });

  const { data: { playeds } = {} } = usePlayedsQuery({
    variables: {
      where: getPlayedFilter(props),
    },
  });

  useEffect(() => {
    if (playeds && playeds.length > 0) {
      const dateSortedPlayedData = playeds.slice().sort((a, b) => {
        var dateA = new Date(a.date),
          dateB = new Date(b.date);
        return dateA - dateB;
      });

      const totalTime = playeds.reduce(
        (total, played) => (total += played.song.duration),
        0
      );

      var date1 =
        startDate & endDate
          ? startDate
          : new Date(dateSortedPlayedData[0].date);
      var date2 = startDate & endDate ? endDate : new Date();

      // To calculate the time difference of two dates
      var Difference_In_Time = date2.getTime() - date1.getTime();

      // To calculate the no. of days between two dates
      var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

      var avgPerDay = totalTime / Difference_In_Days; //ms

      setAvgContext({
        week:
          Difference_In_Days < 7
            ? "Range is less than a week, this number is estimated."
            : Difference_In_Days === 7
            ? "Range is a week, this number is not an avg."
            : "",
        month:
          Difference_In_Days < 30.44
            ? "Range is less than a month, this number is estimated."
            : Difference_In_Days === 30.44
            ? "Range is a month, this number is not an avg."
            : "",
        year:
          Difference_In_Days < 365.24
            ? "Range is less than a year, this number is estimated."
            : Difference_In_Days === 365.24
            ? "Range is a year, this number is not an avg."
            : "",
      });

      var avgMinsPerDay = avgPerDay / (1000 * 60);

      setMinDay(Math.round(avgMinsPerDay));
    }
  }, [playeds, startDate, endDate]);

  return (
    <div>{minDay && <Stopwatch minDay={minDay} avgContext={avgContext} />}</div>
  );
};

export default Graph;
