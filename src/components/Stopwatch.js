import React, { useEffect } from "react";
import { useCountUp } from "react-countup";

const Stopwatch = ({ minDay, avgContext }) => {
  const { week, month, year } = avgContext;
  const { countUp, update } = useCountUp({
    start: 0,
    end: minDay,
    delay: 0,
  });

  const { countUp: countUpWeek, update: updateWeek } = useCountUp({
    start: 0,
    end: minDay * 7,
    delay: 0,
  });

  const { countUp: countUpMonth, update: updateMonth } = useCountUp({
    start: 0,
    end: minDay * 30.44,
    delay: 0,
  });

  const { countUp: countUpYear, update: updateYear } = useCountUp({
    start: 0,
    end: minDay * 365.24,
    delay: 0,
  });

  useEffect(() => {
    update(minDay);
    updateWeek(minDay * 7);
    updateMonth(minDay * 30.44);
    updateYear(minDay * 365.24);
  }, [minDay]);

  const getMins = (mins) =>
    mins % 60 !== 0 ? `${Math.round(mins % 60)}m` : "";

  const getHours = (mins) =>
    Math.floor(mins / 60) !== 0 ? `${Math.floor(mins / 60)}h` : "";

  return (
    <div>
      <h1>{`${getHours(countUp)} ${getMins(countUp)} per day`}</h1>
      <h1>{`${getHours(countUpWeek)} ${getMins(
        countUpWeek
      )} per week. ${week}`}</h1>
      <h1>{`${getHours(countUpMonth)} ${getMins(
        countUpMonth
      )} per month. ${month}`}</h1>
      <h1>{`${getHours(countUpYear)} ${getMins(
        countUpYear
      )} per year. ${year}`}</h1>
    </div>
  );
};

export default Stopwatch;
