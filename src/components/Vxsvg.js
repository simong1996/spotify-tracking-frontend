import React, { useState } from "react";
import { GradientPinkBlue } from "@vx/gradient";
import { scaleOrdinal } from "@vx/scale";
import { Group } from "@vx/group";
import Pie, { ProvidedProps, PieArcDatum } from "@vx/shape/lib/shapes/Pie";
import { animated, useTransition, interpolate } from "react-spring";

const browserNames = ["label", "usage"].filter((k) => k !== "date");
const getBrowserColor = scaleOrdinal({
  domain: browserNames,
  range: [
    "rgba(0,0,0,0.7)",
    "rgba(0,0,0,0.7)",
    "rgba(0,0,0,0.7)",
    "rgba(0,0,0,0.7)",
    "rgba(0,0,0,0.7)",
  ],
});
const defaultMargin = { top: 20, right: 20, bottom: 20, left: 20 };

const Vxsvg = ({
  height,
  width,
  margin = defaultMargin,
  animate = true,
  data = [],
}) => {
  const [selectedBrowser, setSelectedBrowser] = useState(null);
  const innerWidth = width - margin.left - margin.right;
  const innerHeight = height - margin.top - margin.bottom;
  const radius = Math.min(innerWidth, innerHeight) / 2;
  const centerY = innerHeight / 2;
  const centerX = innerWidth / 2;
  const donutThickness = 50;

  return (
    <svg width={width} height={height}>
      {/* <GradientPinkBlue id="vx-pie-gradient" />
      <rect
        rx={14}
        width={width}
        height={height}
        fill="url('#vx-pie-gradient')"
      /> */}
      <Group top={centerY + margin.top} left={centerX + margin.left}>
        <Pie
          data={
            selectedBrowser
              ? data.filter(({ label }) => label === selectedBrowser)
              : data
          }
          pieValue={(d) => d.usage}
          outerRadius={radius}
          innerRadius={radius - donutThickness}
          cornerRadius={3}
          padAngle={0.01}
        >
          {(pie) => (
            <AnimatedPie
              {...pie}
              animate={animate}
              getKey={(arc) => arc.data.label}
              onClickDatum={({ data: { label } }) =>
                animate &&
                setSelectedBrowser(
                  selectedBrowser && selectedBrowser === label ? null : label
                )
              }
              selectedBrowser={selectedBrowser}
              getColor={(arc) => getBrowserColor(arc.data.label)}
            />
          )}
        </Pie>
      </Group>
    </svg>
  );
};

const fromLeaveTransition = ({ endAngle }) => ({
  startAngle: endAngle > Math.PI ? 2 * Math.PI : 0,
  endAngle: endAngle > Math.PI ? 2 * Math.PI : 0,
  opacity: 0,
});

const enterUpdateTransition = ({ startAngle, endAngle }) => ({
  startAngle,
  endAngle,
  opacity: 1,
});

const AnimatedPie = ({
  animate,
  arcs,
  path,
  getKey,
  getColor,
  onClickDatum,
  selectedBrowser,
}) => {
  const transitions = useTransition(arcs, getKey, {
    from: animate ? fromLeaveTransition : enterUpdateTransition,
    enter: enterUpdateTransition,
    update: enterUpdateTransition,
    leave: animate ? fromLeaveTransition : enterUpdateTransition,
  });

  return (
    <>
      {transitions.map(({ item: arc, props, key }) => {
        const [centroidX, centroidY] = path.centroid(arc);
        const hasSpaceForLabel = arc.endAngle - arc.startAngle >= 0.1;

        return (
          <g key={key}>
            <animated.path
              d={interpolate(
                [props.startAngle, props.endAngle],
                (startAngle, endAngle) =>
                  path({
                    ...arc,
                    startAngle,
                    endAngle,
                  })
              )}
              fill={getColor(arc)}
              onClick={() => onClickDatum(arc)}
              onTouchStart={() => onClickDatum(arc)}
              //   onMouseOver={(e) => onClickDatum(arc)}
              //   onMouseOut={(e) => onClickDatum(arc)}
            />
            {hasSpaceForLabel && (
              <animated.g style={{ opacity: props.opacity }}>
                <text
                  fill={selectedBrowser ? "black" : "white"}
                  x={selectedBrowser ? 0 : centroidX}
                  y={selectedBrowser ? 0 : centroidY}
                  dy=".33em"
                  fontSize={9}
                  textAnchor="middle"
                  pointerEvents="none"
                >
                  {getKey(arc)}
                </text>
              </animated.g>
            )}
          </g>
        );
      })}
    </>
  );
};

export default Vxsvg;
