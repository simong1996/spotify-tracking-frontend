import React, { useState } from "react";
import { RadialChart, Hint } from "react-vis";

const Vis = ({ data }) => {
  const [value, setValue] = useState(false);
  return (
    <RadialChart
      className={"donut-chart-example"}
      innerRadius={75}
      radius={140}
      getAngle={(d) => d.theta}
      data={data}
      onValueMouseOver={(v) => {
        setValue({
          name: v.name,
          played: v.theta,
        });
      }}
      onSeriesMouseOut={() => setValue(false)}
      width={300}
      height={300}
      padAngle={0.04}
    >
      {value !== false && <Hint value={value} />}
    </RadialChart>
  );
};

export default Vis;
