import React, { useMemo } from 'react'
import { Bar } from '@vx/shape'
import { Group } from '@vx/group'
import { GradientTealBlue } from '@vx/gradient'
import letterFrequency, {
  LetterFrequency,
} from '@vx/mock-data/lib/mocks/letterFrequency'
import { scaleBand, scaleLinear } from '@vx/scale'

const data = letterFrequency.slice(5)
const verticalMargin = 120

const getLetter = (d) => d.letter
const getLetterFrequency = (d) => Number(d.frequency) * 100

const Vxsvgbar = ({ width, height, events = false }) => {
  const xMax = width
  const yMax = height - verticalMargin

  const xScale = useMemo(
    () =>
      scaleBand({
        rangeRound: [0, xMax],
        domain: data.map(getLetter),
        padding: 0.4,
      }),
    [xMax],
  )
  const yScale = useMemo(
    () =>
      scaleLinear({
        rangeRound: [0, yMax],
        domain: [0, Math.max(...data.map(getLetterFrequency))],
      }),
    [yMax],
  )

  return width < 10 ? null : (
    <svg width={width} height={height}>
      {/* <GradientTealBlue id="teal" /> */}
      <rect width={width} height={height} fill="url(#teal)" rx={14} />
      <Group top={verticalMargin / 2}>
        {data.map((d) => {
          const letter = getLetter(d)
          const barWidth = xScale.bandwidth()
          const barHeight = yMax - yScale(getLetterFrequency(d))
          const barX = xScale(letter)
          const barY = yMax - barHeight
          console.log(barHeight, barX, barY)
          return (
            <Bar
              key={`bar-${letter}`}
              x={barX}
              y={barY}
              width={barWidth}
              height={barHeight}
              fill="rgba(23, 233, 217, .5)"
              onClick={() => {
                console.log('hey', d)
                if (events)
                  alert(`clicked: ${JSON.stringify(Object.values(d))}`)
              }}>
              <animate
                attributeName="height"
                from={0}
                to={barHeight}
                dur="2s"
                fill="freeze"
              />
            </Bar>
          )
        })}
      </Group>
    </svg>
  )
}

export default Vxsvgbar
