import React from 'react'
import ParentSize from '@vx/responsive/lib/components/ParentSize'
import Vxsvg from './Vxsvg'

const Vx = ({ data }) => {
  return (
    <ParentSize>
      {({ width, height }) => (
        <Vxsvg width={width} height={height} data={data} />
      )}
    </ParentSize>
  )
}

export default Vx
